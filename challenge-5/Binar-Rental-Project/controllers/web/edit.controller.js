const { default:axios } = require("axios")

function edit(req, res) {

    axios.get(`http://localhost:9999/api/cars/${req.params.id}`).then(
        (response) => axios.get(`http://localhost:9999/api/size`).then((reponseSize) => {
            res.render('../views/form_edit', {cars:response.data.data, size:reponseSize.data.data})
        })
    )

}

module.exports = {
    edit
}
