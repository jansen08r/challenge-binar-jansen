const { default: axios } = require("axios")

function create(req, res) {
    axios.get(`http://localhost:9999/api/size`).then((reponseSize) => {
        res.render('../views/form', {size:reponseSize.data.data})
    })
}

module.exports = {
    create
}
