const {default:axios} = require('axios')

function index(req,res) {
    axios.get('http://localhost:9999/api/cars').then((response) => {
        console.log(response.data.data);
        res.render('../views/index',{Cars:response.data.data})
    })
    
}

module.exports = {index}