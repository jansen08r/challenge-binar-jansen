const db = require('../../models')
// const tbl_cars = require('../../models/tbl_cars')
const tbl_cars = db.tbl_cars;

async function getAllCars(req, res) {
    try{
        let cars = await tbl_cars.findAll()
        // console.log(cars);

        res.status(200).json({
            message:"success",
            data: cars
        })
    }
    catch(error){
        console.log(error);
    }
}

async function newCars(req, res) {
    try {
        let carReq = req.body

        let car = {
            name: carReq.name,
            harga: carReq.harga,
            url_image:carReq.url_image,
            id_size:carReq.id_size,
            createdAt:new Date(),
            updatedAt:new Date()
        }

        tbl_cars.create(car)
            .then(() => {
            res.status(200).json({
                message: "success",
                data : car
            })
        }).catch((error) => console.log(error));

        // sumber promise chain -> async await : https://advancedweb.hu/how-to-refactor-a-promise-chain-to-async-functions/
        // let responseCar = await tbl_cars.create(car);
        // res.status(200).json({
        //         message: "success",
        //         data : car
        //     })
        
    }
    catch(error){
        console.log(error);
    }
}

async function showCar(req, res) {
    try{
        let id = +(req.params.id)
        let car = await tbl_cars.findOne({
            where : {id}
        })

        res.status(200).json({
            message: "success",
            data : car
        })
    }
    catch(error){
        console.log(error)
    }
}

async function editCars(req, res) {

    try{
        let carReq = req.body
        let idCar = +(req.params.id)

        let car = {
            name: carReq.name,
            harga: carReq.harga,
            url_image:carReq.url_image,
            id_size:carReq.id_size,
            createdAt:new Date(),
            updatedAt:new Date()
        }
        tbl_cars.update(car, {
            where : {id:idCar}
        }).then(() => {
            res.status(200).json({
                message:"success",
                data : []
            })
        }).catch((error) => {
            console.log(error)
        })
    }
    catch(error){
        console.log(error)
    }
}

async function deleteCar(req, res) {
    try{
        let id = +(req.params.id)
        tbl_cars.destroy({
            where : {id:id}
        }).then(() => {
            res.status(200).json({
                message: "success",
                data : id
            })
        }).catch((err) => {
            console.log(err);
        })
    }
    catch(error){
        console.log(error)
    }
}

module.exports = {
    getAllCars,
    newCars,
    editCars,
    showCar,
    deleteCar
}
