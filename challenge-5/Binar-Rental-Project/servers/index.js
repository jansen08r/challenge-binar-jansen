const express = require('express')
const app = express()
const path = require('path')

app.use(express.json())
app.use(express.urlencoded())

//disesuain sama pemanggilan css di html nya, posisi file static yang diambil semua didalem views ( dlm konteks ini )
app.use(express.static(path.join(__dirname, '../views')))


const webRouter = require('./../routes/web.router')
const apiRouter = require('./../routes/api.router')

const PORT = 9999;
app.set('view engine', 'ejs')

app.use('/', webRouter) // tampilan web
app.use('/api', apiRouter) // tampilan api


app.listen(PORT, () => {
    console.log("Berjalan di", PORT);
})