'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('tbl_cars', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      // id_size: {
      //   type: Sequelize.INTEGER,
      //   references:{
      //     model:'tbl_sizes',
      //     key:'id'
      //   }
      // },
      name: {
        type: Sequelize.STRING
      },
      harga: {
        type: Sequelize.INTEGER
      },
      url_image: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('tbl_cars');
  }
};