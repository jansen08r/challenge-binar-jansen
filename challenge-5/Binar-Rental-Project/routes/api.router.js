const express = require('express')
const router = express.Router()
const {getAllSize} = require('../controllers/api/size.controller')
const {getAllCars, newCars, showCar, editCars, deleteCar} = require('../controllers/api/cars.controller')


router.get('/size', getAllSize)
router.get('/cars', getAllCars)
router.post('/cars', newCars)
router.get('/cars/:id', showCar)
router.post('/cars/:id', editCars) //diganti dari put ke post dulu ( disuruh Ka Reka karena put ga support di html )
// router.delete('/cars/:id', deleteCar)
router.get('/cars/delete/:id', deleteCar)


module.exports = router
// nama harus router gabole asal, beda sama fungsi yang bisa namanya macem macem
// kalo gapake { } otomatis dia jadi default export, bisa dipanggil pake nama seenaknya sama gausah pake { } pas import