const express = require('express')
const router = express.Router()
const{index} = require('../controllers/web/index.controller')
const{create} = require('../controllers/web/create.controller')
const{edit} = require('../controllers/web/edit.controller')


router.get('/', index)
router.get('/create', create)
router.get('/edit/:id', edit)


module.exports = router
// kalo gapake { } otomatis dia jadi default export, bisa dipanggil pake nama seenaknya sama gausah pake { } pas import