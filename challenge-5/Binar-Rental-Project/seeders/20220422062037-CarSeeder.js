'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('tbl_cars', [
      {
      name:'Avanza',
      harga:500000,
      url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650609410/challenge-5/avanza_fkpqn4.png",
      createdAt: new Date(),
      updatedAt: new Date(),
      id_size:2
      },
      {
        name:'Mazda 2',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611199/challenge-5/mazda2_nfftxz.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        name:'Kia Senot',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611196/challenge-5/kiaSonet1_wnliob.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        name:'Suzuki Ignis',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611196/challenge-5/ignis1_hvibx3.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:1
      },
      {
        name:'Honda Accord',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611198/challenge-5/accord1_ivf7db.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:2
      },
      {
        name:'Honda Camry',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611198/challenge-5/camry1_pdn1ma.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:2
      },
      {
        name:'Mitsubishi Pajero',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611197/challenge-5/pajero1_vfzfzl.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:3
      },
      {
        name:'Toyota Fortuner',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611197/challenge-5/fortuner1_ak1s5x.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:3
      },
      {
        name:'Toyota Alphard',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611197/challenge-5/alphard1_ggmr1x.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:3
      },
      {
        name:'Toyota Vellfire',
        harga:500000,
        url_image:"https://res.cloudinary.com/jansencloud1/image/upload/v1650611197/challenge-5/vellfire1_rlu6l2.jpg",
        createdAt: new Date(),
        updatedAt: new Date(),
        id_size:3
      },
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
