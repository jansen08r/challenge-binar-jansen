'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class tbl_cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // ! ditambahin manual, beda sama yg di migration ( itu cuma tabel doang ), relasi tapi buat manual, biar nanti bisa nyambung
    static associate(models) {
      this.belongsTo(models.tbl_size, {
        foreignKey : 'id_size'
      })
    }
  }
  tbl_cars.init({
    name: DataTypes.STRING,
    harga: DataTypes.INTEGER,
    url_image: DataTypes.STRING //harus diisi manual karena tabel nya hasil migrasi add column ( ga dari awal pas bikin tabel )
                               // efeknya kalo ga diisi nanti ga muncul di postman nya
                              // ! Ditambahin soalnya url image pas dibuat di awal migration belum, jadi ditambahin manual di akhir
  }, {
    sequelize,
    modelName: 'tbl_cars',
  });
  return tbl_cars;
};