# Challenge 7 

## Akses Melalui Link Berikut :

[https://challenge-7-jansen.netlify.app](https://challenge-7-jansen.netlify.app/)

## Atau Ikuti Langkah Dibawah Ini  :

### `1. npm install`
Pindah posisi file menggunakan command `cd challenge-7` terlebih dahulu, setelah itu ketik `npm install` di terminal.

### `2. npm start`

Menjalankan di [http://localhost:3000](http://localhost:3000) di browser.

### `3. npm run build` ( opsional )

Membuat folder bernama build yang bisa digunakan untuk deploy ke Netlify.
