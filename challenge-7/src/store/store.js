import { createStore, compose, applyMiddleware } from 'redux'
import reducer from './reducers' // kok bisa tau ke reducers/index 
import thunk from 'redux-thunk';

// const store = createStore(reducer)
const store = createStore(reducer, compose(applyMiddleware(thunk)))


export default store