import axios from "axios";

export const filter = (data) => async (dispatch) => {
    try{
        const response = await axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json');
        dispatch({
            type: "FILTER",
            payload:{
                driver: data.driver,
                date: data.date,
                time: data.time,
                passenger: data.passenger,
                axios: response.data
            }
        })
    }
    catch(err){
        console.log(err)
    }
}