import React from 'react';
import logoBinar from '../images/logobinar.png'
import { useNavigate } from "react-router-dom";

const Navbar = () => {
    
    const navigate = useNavigate();

    return (
        <>
            <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top" style={{paddingLeft: "10vw", paddingRight:'10vw'}}> 
                <div class="container-fluid px-0">
                <a class="navbar-brand" href="#" >
                   <img src={logoBinar} class="pe-2" alt="" onClick={()=>navigate("/")} />
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="#">Our Services</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Why Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Testimony</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#"> FAQ </a>
                    </li>
                    <li class="nav-item">
                        <a class="btn btn-success btn-sm button1 nav-link active" href="#" role="button"
                        >Register</a>
                    </li>
                    
                    </ul>
                    
                </div>
                </div>
            </nav>
        </>
    );
}

export default Navbar;
