function filterCarByAvailability(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  console.log(cars); // ini dia array of object

  // Tempat penampungan hasil
  const result = [];
  let indexBaru = 0;

  // Tulis code-mu disini

  for (let index = 0; index < cars.length; index++) {
    if( cars[index].available === true){
      result[indexBaru] = cars[index];
      indexBaru++;
    }
    
  }

  // Rubah code ini dengan array hasil filter berdasarkan availablity
  return result;
}
