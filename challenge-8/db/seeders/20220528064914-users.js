'use strict';

const { Op } = require("sequelize");
const bcrypt = require("bcryptjs");
const { Role } = require("../../app/models");

const names = [
  "Johnny",
  "Fikri",
  "Brian",
  "Ranggawarsita",
  "Jayabaya",
]

module.exports = {
  async up (queryInterface) {
    const password = "123456";
    const encryptedPassword = bcrypt.hashSync(password, 10);
    const timestamp = new Date();

    const role = await Role.findOne({
      where: {
        name: "ADMIN",
      }
    })

    const users = names.map((name) => ({
      name,
      email: `${name.toLowerCase()}@binar.co.id`,
      encryptedPassword,
      roleId: role.id, 
      createdAt: timestamp,
      updatedAt: timestamp,
    }))

    const userBiasa = {
      name : "jansen",
      email : "jansen@mail.com",
      encryptedPassword : bcrypt.hashSync("090900", 10),
      roleId : 1,
      createdAt: timestamp,
      updatedAt: timestamp,
    }

    const gabungan = [...users, userBiasa]
    console.log(gabungan, "ini GABUNGANNNNN")

    await queryInterface.bulkInsert('Users',gabungan, {});
  },

  async down (queryInterface) {
    await queryInterface.bulkDelete('Users', { name: { [Op.in]: names } }, {});
  }
};
