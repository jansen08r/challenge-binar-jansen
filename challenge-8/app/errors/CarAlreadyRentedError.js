const ApplicationError = require("./ApplicationError");

class CarAlreadyRentedError extends ApplicationError {
  // constructor() {
  //   super(`already rented!!`);
  // }

  // get details() {
  //   return { car : this.car }; // kok bisa gini ? dari github copilot
  // }
}

module.exports = CarAlreadyRentedError;
