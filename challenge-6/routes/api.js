var express = require('express');
var router = express.Router();
const authJWT = require('../middleware/jwtauthenticate')

const { getAllAvailableCars ,getAllCar, softDeleteCar, getCar, deleteCar, updateCar, createCar } = require('../controllers/cars.controller')
const {getAllUsers, createUser, updateUser, deleteUser, getUser, getUserByToken, createAdmin} = require('../controllers/user.controller')
const { login } = require('../controllers/auth.controller');
const roleAuth = require('../middleware/roleAuth');
const superRoleAuth = require('../middleware/superRoleAuth')

// Cars
router.get('/v1/availableCars', getAllAvailableCars);
router.get('/v1/cars', getAllCar);
router.get('/v1/cars/:id', getCar);
router.post('/v1/cars', [authJWT, roleAuth], createCar);
router.put('/v1/cars/:id',[authJWT, roleAuth], updateCar)
router.delete('/v1/cars/:id',[authJWT, roleAuth], deleteCar);
router.put('/v1/cars/softDelete/:id',[authJWT, roleAuth], softDeleteCar)

// Users
router.post('/v1/users/auth',login) // Poin 1, login
router.post('/v1/users/admin', [authJWT, roleAuth, superRoleAuth], createAdmin); // Poin 2, create Admin ( hanya super admin yang boleh ngisi )
router.post('/v1/users', createUser); // Poin 3, create member biasa 
router.get('/v1/users', [authJWT, roleAuth], getAllUsers);
// router.get('/v1/users/:id', [authJWT, roleAuth], getUser); bisa di-bruteforce
// router.put('/v1/users/:id', [authJWT, roleAuth], updateUser);
// router.delete('/v1/users/:id', [authJWT, roleAuth], deleteUser);
router.get('/v1/user', [authJWT] ,getUserByToken) // getbyid digantikan dengan token, biar ga di-bruteforce 

module.exports = router;
