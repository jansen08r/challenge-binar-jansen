const db = require('../models');
const tbl_user = db.tbl_users 
const jwt = require('jsonwebtoken');
const jwt_decode  = require('jwt-decode')
require('dotenv').config()

const roleAuth = async( req, res, next) => {
    const authHeader = req.headers['authorization']

    if(authHeader){
        const token = authHeader.split(' ')[1];
        console.log("ini token : ", token);

        let decoded = jwt_decode(token);

        const data = await tbl_user.findOne({
            where:{id:decoded.id}, attributes:{exclude:['password']} //attributes password gaakan di tampilin di data ( pas di res.status(200).json({data}))
        })

        if((data.role === 'admin') || (data.role === 'super')){
            next()
        }
        else{
            res.status(401).json({
                message: "Anda bukan role nya !"
            })
        }}
    else{
        res.sendStatus(401)
    }
} 

module.exports = roleAuth