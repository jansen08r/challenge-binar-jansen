const jwt = require('jsonwebtoken');
require('dotenv').config()

const authJWT = ( req, res, next) => {
    const authHeader = req.headers['authorization'] // "a" nya kecil

    if(authHeader){
        const token = authHeader.split(' ')[1];
        console.log("ini token :", token);
        // const secretKey = '4C617E204D9C1BF3ABCB445DF3A808C9F5C18C18668B29C3399D38946BC35BD8'
        const secretKey = process.env.JWT_SECRET_DEV

        jwt.verify(token,secretKey, (err, user) => {
            if(err) {
                return res.status(500).json({
                    message: err.message
                })
            }
            req.user = user;
            console.log("user adalah :", user);

            next();
        })
    }
    else{
        res.status(401).json({
            message: 'token tidak sesuai'
        })
    }
} 

module.exports = authJWT