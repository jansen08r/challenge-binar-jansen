const db = require('../models');
const tbl_cars = db.tbl_cars
const tbl_users = db.tbl_users 
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const jwt_decode  = require('jwt-decode')

async function getAllAvailableCars(req, res){
    try{
        let response = await tbl_cars.findAll({
            where:{deletedAt: null, isAvailable:true }
        });
        res.status(201).json({
            message:"success",
            data: response
        }) 
    }
    catch (err) {
        console.log(err)
    }
}

async function getAllCar(req, res){
    try{
        let response = await tbl_cars.findAll({
            where:{deletedAt: null }
        });
        res.status(201).json({
            message:"success",
            data: response
        }) 
    }
    catch (err) {
        console.log(err)
    }
}

async function getCar(req, res){
    try{
        let id = +(req.params.id)
        let response = await tbl_cars.findOne({
            where:{id:id, deletedAt:null} // multiple where, sumber : https://stackoverflow.com/questions/10807765/node-js-sequelize-multiple-where-query-conditions
        }); 

        res.status(201).json({
            message:"success",
            data: response
        }) 
    }
    catch (err) {
        console.log(err)
        res.status(201).json({
            message:"success",
            data: err.message
        }) 
    }
}

async function createCar(req, res){
    try{

        const authHeader = req.headers['authorization']

        if(authHeader){
            const token = authHeader.split(' ')[1];
            console.log("ini token : ", token);

            let decoded = jwt_decode(token);
            const data = await tbl_users.findOne({
                where:{id:decoded.id}, attributes:{exclude:['password']} //attributes password gaakan di tampilin di data ( pas di res.status(200).json({data}))
            })

            let carReq = req.body
            let car = {
                nama_mobil:carReq.nama_mobil,
                isAvailable:carReq.isAvailable,
                createdBy: data.name,
                createdAt: new Date(), 
                updatedAt: new Date()
            }
            await tbl_cars.create(car)
                res.status(201).json({
                    message:"success",
                    data:car
            })
        }
    }
    catch(err){
        console.log(err);
    }
}

async function updateCar(req, res) {

    try{
        const authHeader = req.headers['authorization']

        if(authHeader){

            const token = authHeader.split(' ')[1];
            console.log("ini token : ", token);

            let decoded = jwt_decode(token);
            
            let carReq = req.body
            let id = +(req.params.id)
            // data bentuknya array dari akun user yang ketemu
            const data = await tbl_users.findOne({
                where:{id:decoded.id}, attributes:{exclude:['password']} //attributes password gaakan di tampilin di data ( pas di res.status(200).json({data}))
            })

            let car = {
                nama_mobil:carReq.nama_mobil,
                isAvailable: carReq.isAvailable,
                updatedBy: data.name,
                createdAt: new Date(),
                updatedAt: new Date()
            }

            tbl_cars.update(car, {
                where : {id:id}
            }).then(() => {
                res.status(201).json({
                    message:"berhasil update",
                    data : car
                })
            }).catch((error) => {
                console.log(error)
            })
        }
    }
    catch(error){
        console.log(error)
    }
}

async function deleteCar(req, res) {
    try{
        let id = +(req.params.id)
        tbl_cars.destroy({
            where : {id:id}
        }).then(() => {
            res.status(200).json({
                message: "berhasil di delete",
                data : id
            })
        }).catch((err) => {
            console.log(err);
        })
    }
    catch(error){
        console.log(error)
    }
}

async function softDeleteCar(req, res) {
    try{
        const authHeader = req.headers['authorization']

        if(authHeader){
            const token = authHeader.split(' ')[1];
            console.log("ini token : ", token);

            let decoded = jwt_decode(token);

            let carReq = req.body
            let id = +(req.params.id)

            const data = await tbl_users.findOne({
                where:{id:decoded.id}, attributes:{exclude:['password']} //attributes password gaakan di tampilin di data ( pas di res.status(200).json({data}))
            })
    
            let car = {
                deletedAt: new Date(),
                deletedBy: data.name,
                createdAt: new Date(),
                updatedAt: new Date()
            }
                    
            tbl_cars.update(car, {
                where : {id:id}
            }).then(() => {
                res.status(200).json({
                    message:"berhasil di-softdelete",
                    data : car
                })
            }).catch((error) => {
                console.log(error)
            })
        }
    }
    catch(error){
        console.log(error)
    }
}

module.exports = {
    getAllCar,
    getCar,
    createCar,
    updateCar,
    deleteCar,
    softDeleteCar,
    getAllAvailableCars
}