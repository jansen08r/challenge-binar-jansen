const db = require('../models');
const tbl_user = db.tbl_users 
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken')
const jwt_decode  = require('jwt-decode')

async function getAllUsers(req, res){
    try{
        let response = await tbl_user.findAll({
            attributes:{exclude:['password','email']}
        });
        res.status(200).json({
            message:"success",
            data: response
        }) 
    }
    catch (err) {
        console.log(err)
    }
}

async function getUser(req, res){
    try{
        let id = +(req.params.id)
        let response = await tbl_user.findOne({
            where:{id}
        });
        res.status(200).json({
            message:"success",
            data: response
        }) 
    }
    catch (err) {
        console.log(err)
    }
}

async function createUser(req, res){
    try{
        let userReq = req.body;
        let salt = 'AbUIcdD'
        const hashPassword = await bcrypt.hash(userReq.password+salt, 12)
        console.log("password setelah di hash menjadi :", hashPassword);

        
        let user = {
            name:userReq.name,
            email:userReq.email,
            password:hashPassword,
            role:"member",
            createdAt: new Date(),
            updatedAt: new Date()
        }

        await tbl_user.create(user)
        res.status(201).json({
            message:"success",
            data:user
        })
    }
    catch(err){
        console.log(err);
    }
}

async function createAdmin(req, res){
    try{
        let userReq = req.body;
        let salt = 'AbUIcdD'
        const hashPassword = await bcrypt.hash(userReq.password+salt, 12)
        console.log("password setelah di hash menjadi :", hashPassword);

        let user = {
            name:userReq.name,
            email:userReq.email,
            password:hashPassword,
            role:"admin",
            createdAt: new Date(),
            updatedAt: new Date()
        }

        await tbl_user.create(user)
        res.status(201).json({
            message:"success",
            data:user
        })
    }
    catch(err){
        console.log(err);
    }
}

async function updateUser(req, res) {

    try{
        let userReq = req.body
        let id = +(req.params.id)
        let salt = 'AbUIcdD'
        const hashPassword = await bcrypt.hash(userReq.password+salt, 12)
        console.log("password setelah di hash menjadi :", hashPassword);

        let user = {
            name:userReq.name,
            email:userReq.email,
            password:hashPassword,
            role:userReq.role,
            createdAt: new Date(),
            updatedAt: new Date()
        }

        tbl_user.update(user, {
            where : {id:id}
        }).then(() => {
            res.status(201).json({
                message:"berhasil update",
                data : user
            })
        }).catch((error) => {
            console.log(error)
        })
    }
    catch(error){
        console.log(error)
    }
}

async function deleteUser(req, res) {
    try{
        let id = +(req.params.id)
        tbl_user.destroy({
            where : {id:id}
        }).then(() => {
            res.status(201).json({
                message: "berhasil di delete",
                data : id
            })
        }).catch((err) => {
            console.log(err);
        })
    }
    catch(error){
        console.log(error)
    }
}

async function getUserByToken(req, res) {
    try{
        const authHeader = req.headers['authorization']

        if(authHeader){
            const token = authHeader.split(' ')[1];
            console.log("ini token : ", token);

            let decoded = jwt_decode(token);
            let id = decoded.id

            const data = await tbl_user.findOne({
                where:{id:id}, attributes:{exclude:['password','email']} //attributes password gaakan di tampilin di data ( pas di res.status(200).json({data}))
            })

            res.status(200).json({
                message:`Ini nama dari token ${data}, namanya ${data.name} dengan role ${data.role}`,
                data // disini udah gaakan ada password karena udah di exclude diatas
            })
        }
    }
    catch(err){
        console.log(err);
        res.status(401).json({
            message: err.message
        })
    }
    
}

module.exports = {
    getAllUsers,
    getUser,
    createUser,
    updateUser,
    deleteUser,
    getUserByToken,
    createAdmin
}