const db = require('../models');
const tbl_user = db.tbl_users
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config()

async function login(req, res){
    try{
        const body = req.body
        if((body.email !== '') && (body.password !== '')){

            const user = await tbl_user.findOne({
                where:{
                    email: body.email
                }
            })
            
            // ini buat encrypt password
            let salt = 'AbUIcdD'
            const matchPass = await bcrypt.compare(body.password+salt, user.password)
            console.log("matchPass adalah :",matchPass );

            if(!matchPass){
                return res.status(401).json({
                    message:"username dan/atau password tidak sesuai"
                })
            }

            const secretKey = process.env.JWT_SECRET_DEV
            const expireKey = '24h'

            const tokenAccess = jwt.sign(
                {
                id: user.id
                },
                secretKey,
                {
                    algorithm: "HS256",
                    expiresIn: expireKey
                }
            )

            return res.status(200).json({
                message: "Sesuai !",
                token: tokenAccess
            })
        }
    }
    catch(err){
        res.status(500).json({
            message: err
        })
    }
}

module.exports={
    login
}