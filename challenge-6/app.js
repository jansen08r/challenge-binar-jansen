var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json')

var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');
var apiRouter = require('./routes/api')

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
// app.use('/users', usersRouter);

// API Router
app.use('/api', apiRouter)
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// sumber kenapa export json dari postman ke swagger biar bisa jadi : https://stackoverflow.com/questions/31299098/how-can-i-generate-swagger-based-off-of-existing-postman-collection#:~:text=APIMatic%20API%20Transformer%20can%20process%20a%20Postman%20collection%20(v1%20or%20v2)%20as%20an%20input%20format%20and%20produce%20Swagger%201.2%20or%202.0%2C%20and%20now%20OpenAPI%203.0.0%20as%20output.

module.exports = app;
