'use strict';

const bcrypt = require("bcryptjs/dist/bcrypt");
// sequelize db:seed --seed [nama file] kak <-- cara nge seed


module.exports = {
  async up (queryInterface, Sequelize) {
   let salt = "AbUIcdD";
   const hashPassword = await bcrypt.hash('090900' + salt, 12); //input manual hardcode

  await queryInterface.bulkInsert('tbl_users', [
       {
        name: 'Jansen',
        email:'jansenc@mail.com',
        password:hashPassword,
        role:'super',
        createdAt:new Date(),
        updatedAt:new Date()
        },
      ], {});

  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
