class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    
    <div class="card shadow h-100">
      <div class="card-body">
        <img src="${this.image}" class="img-fluid" style="height: 300px;object-fit: cover; width: 100%" />
        <h5 class="car-title pt-2">${this.manufacture}/${this.model}</h5>
        <h2 class="car-price">Rp ${Intl.NumberFormat('de-DE').format(this.rentPerDay)} / hari</h2>
        <p class="car-subtitle">${this.description}</p>
        
      </div>
      <div class="car-detail px-3 pb-3">
          <div class="detail-item pb-2"> 
            <img src="./images/iconUser.png" alt="">
            ${this.capacity}
          </div>
          <div class="detail-item pb-2">
            <img src="./images/iconSetting.png" alt="">
            ${this.transmission}
          </div>
          <div class="detail-item pb-2">
            <img src="./images/iconCalender.png" alt="">
            ${this.year}
          </div>
        </div>
      <div class="px-3 pb-3">
        <button type="button" class="btn btn-success w-100" style="position:relative; bottom:0px">Pilih Mobil</button>
      </div>
    </div>
  

    `;
  }
}
