class App {
  constructor() {
    this.carContainerElement = document.getElementById("cars-container");
    this.buttonSubmitData = document.getElementById('submitFilter')
    this.tipeDriver = document.getElementById('tipeDriver')
    this.dateSewa = document.getElementById('dateSewa')
    this.waktuJemput = document.getElementById('waktuJemput')
    this.jumlahPenumpang = document.getElementById('jumlahPenumpang')
  }

  async init() {
    await this.load();

    // Register click listener
    // this.clearButton.onclick = this.clear;
    this.buttonSubmitData.onclick = this.run;

    this.run 
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div"); // daia buat div, terus di dalemnya baru di render semua yang datanya
      node.classList.add("col-lg-4","p-2")
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);

    this.buttonSubmitData.addEventListener('click',async (e) => {
      let tipeDriverVal = this.tipeDriver.options[this.tipeDriver.selectedIndex].value
      let dateSewaVal = this.dateSewa.value
      let waktuJemputVal = this.waktuJemput.options[this.waktuJemput.selectedIndex].value
      // let waktuJemputVal = this.waktuJemput.value
      let jumlahPenumpang = this.jumlahPenumpang.value
      let aku;
      
      // this.tipeDriver.options[this.tipeDriver.selectedIndex].value untuk dropdown

      this.clear();

      let cars = await Binar.listCars((car) => {
        let result = true;
       
        let dateTime = dateSewaVal + "T" + waktuJemputVal;
        if((!isNaN(Date.parse(dateTime))) && (!isNaN(parseInt(jumlahPenumpang)))){
          result = (car.availableAt.getTime() <= Date.parse(dateTime)) && (car.capacity >= parseInt(jumlahPenumpang));
        }

        else if(!isNaN(Date.parse(dateTime))){
          result = car.availableAt.getTime() >= Date.parse(dateTime)
        }

        else if(!isNaN(parseInt(jumlahPenumpang))){
          result = car.capacity >= parseInt(jumlahPenumpang)
        }
       
        return result
      })
      console.log(cars.filter(e => e.available));
      
      Car.init(cars.filter(e => e.available ));

    })
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
