const http = require('http');
const {
    PORT = 3000
} =  process.env;

const fs = require('fs');
const path = require('path')
const PUBLIC_DIRECTORY = path.join(__dirname, '../public');

function renderHTML(htmlFileName){
    const htmlFilepath = path.join(PUBLIC_DIRECTORY, htmlFileName)
    return fs.readFileSync(htmlFilepath, 'utf-8')
}

function onRequest(req,res) {
    switch(req.url){
        case '/':
            res.writeHead(200, {'content-Type': 'text/html'})
            res.end(renderHTML('index.html'))
        return; //gada return dia ga ngejalanin default, jadi mending ada in aja
        case "/cars":
            res.writeHead(200, {'Content-Type': 'text/html'})
            res.end(renderHTML('search.html'))
        return;
        default:
            // if(req.url.indexOf('css') != -1) { //req.url has the pathname, check if it contains '.css' 
            // console.log(__dirname)
            // console.log(PUBLIC_DIRECTORY)
          const FilePath = path.join(PUBLIC_DIRECTORY, `${req.url}`)
          // console.log(FilePath);
          fs.readFile(FilePath, function (err, data) {
            // res.writeHead(200, {'Content-Type': ['text/css','img/png']});
            // console.log(req.url,"==="); // clog apa aja yang diminta sama html nya ( kepake )
            res.write(data);
            res.end();
            
        });
        }
        // else{
        //     res.writeHead(404)
        //     res.end("File Not Found")
        // }
    }
// }

const server = http.createServer(onRequest);

server.listen(PORT, 'localhost', () =>{
    console.log("server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
})
